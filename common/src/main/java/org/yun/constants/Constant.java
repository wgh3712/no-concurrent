package org.yun.constants;

/**
 * @author liyunfeng31
 */
public class Constant {


    public static final Long SUCCESS = 1L;

    public static final int ONLINE = 1;

    /**
     * 缓存满库存量1000
     */
    public static final int FULL_STOCK = 1000;


    /**
     * 默认的兜底奖品 谢谢参与 (库存-1)
     */
    public static final String THANKS = "THANKS";

    /**
     * 正常奖品
     */
    public static final String LUCK = "LUCK";

    /**
     * 谢谢参与
     */
    public static final int PRIZE_THANKS = -1;

    /**
     * 普通奖品
     */
    public static final int PRIZE_NORMAL = 1;

    /**
     * 具有唯一性的奖品
     */
    public static final int PRIZE_UNIQUE = 2;

    /**
     * 无库存标识
     */
    public static final String NO_STOCK = "-1";

    /**
     * 无库存标识
     */
    public static final Long NO_STOCK_ = -1L;

    /**
     * 重复参加
     */
    public static final Long PARTICIPATED = -2L;

    /**
     * 字符串连接符
     */
    public static final String JOIN = "@_@";


    /**
     * 最小红包值 1分钱
     */
    public static final double MIN_RED_PACK = 1;


}
