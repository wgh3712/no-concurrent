package org.yun.constants;

/**
 * @author liyunfeng31
 */
public class ScriptConstant {


    public static final String SEC_KILL_SCRIPT =
            "local skuStock = KEYS[1] " +
            "local skuUids = KEYS[2] " +
            "local isIn = redis.call('SISMEMBER', skuUids, ARGV[1]) " +
            "if isIn > 0 then " +
            "  return -2 " +
            "end " +
            "local stock = redis.call('GET', skuStock) " +
            "if not stock or tonumber(stock) < tonumber(ARGV[2]) then " +
            "  return -1 " +
            "end " +
            "redis.call('DECRBY', skuStock, ARGV[2]) " +
            "redis.call('SADD', skuUids, ARGV[1]) " +
            "return 1";

    public static final String DECR_SCRIPT =
            "local num = redis.call('GET', KEYS[1]) " +
            "if not num or tonumber(num) <= 0 then " +
            "    return -1 " +
            "end " +
            "redis.call('DECR', KEYS[1]) " +
            "return 1 ";


    public static final String COUPON_SCRIPT =
            "local num = redis.call('GET', KEYS[1]) " +
            "if not num or tonumber(num) <= 0 then " +
            "    return '-1' " +
            "end " +
            "local item = redis.call('RPOP', KEYS[2]) " +
            "if item ~= nil then " +
            "    redis.call('DECR', KEYS[1]) " +
            "    local remainder = redis.call('LLEN', KEYS[2]) " +
            "    if tonumber(remainder) == tonumber(ARGV[1]) then " +
            "        redis.call('LPUSH', KEYS[3], ARGV[2]) " +
            "    end " +
            "    return item " +
            "end ";





    public static final String UNLOCK_SCRIPT = "if (redis.call('get', KEYS[1]) == ARGV[1]) then " +
            "redis.call('del', KEYS[1]) " +
            "   return 1 " +
            "else  " +
            "   return 0 " +
            "end";


    public static final String  CAS_GRAB_RED_SCRIPT =
            "local redKey = KEYS[1] " +
            "local usersKey = KEYS[2] " +
            "local listKey = KEYS[3] " +
            "local userId = ARGV[1] " +
            "if redis.call('HEXISTS', usersKey, userId) ~= 0 then " +
            "   return -2  " +
            "end " +


            "local oldNum = redis.call('HGET', redKey, 'remainingNum') " +
            "if oldNum == 0 then " +
            "    return -1 " +
            "end " +
            "local newRed = cjson.decode(ARGV[2]) " +
            "if tonumber(newRed['remainingNum']) ~= (oldNum - 1) then " +
            "    return 0  " +
            "end " +

            "redis.call('HSET', usersKey, userId, newRed['grabAmount']) " +
            "redis.call('HMSET', redKey,'remainingAmount', newRed['remainingAmount'],'remainingNum', newRed['remainingNum']) " +
            "local red = {userId = userId, redId=newRed['redId'], amount=newRed['amount']} " +
            "redis.call('LPUSH', listKey, cjson.encode(red) ) " +
            "return  1 ";


    public static final String POP_GRAB_RED_SCRIPT =
            "local grabKey = KEYS[1] " +
            "local usersKey = KEYS[2] " +
            "local recordKey = KEYS[3] " +
            "local userId = ARGV[1] " +
            "if redis.call('HEXISTS', usersKey, userId) ~= 0 then " +
            "    return -2 " +
            "end   " +
            "local grabRed = redis.call('RPOP', grabKey) " +
            "if grabRed == nil then " +
            "    return -1 " +
            "end " +

            "local rp = cjson.decode(grabRed) " +
         /* "redis.log(redis.LOG_WARNING,'-------------------------------------') "+
           "redis.log(redis.LOG_WARNING,rp['tId']) "+
            "redis.log(redis.LOG_WARNING,rp['amount']) "+*/
            "redis.call('HSET', usersKey, userId, rp['amount']) " +
            "local record = {userId = userId, tId = rp['tId'], amount = rp['amount']} " +
            "redis.call('LPUSH', recordKey, cjson.encode(record)) " +
            "return tonumber(rp['amount'] ) ";
}
