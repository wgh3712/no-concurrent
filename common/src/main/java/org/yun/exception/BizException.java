package org.yun.exception;

import lombok.Getter;

import java.util.Objects;

/**
 * @author liyunfeng31
 */
public class BizException extends RuntimeException {

    @Getter
    private IErrorCode errorCode;

    @Getter
    private String message;

    public BizException(Throwable cause) {
        super(cause);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(IErrorCode errorCode) {
        Objects.requireNonNull(errorCode, " error code  can not be null");
        this.errorCode = errorCode;
        this.message = errorCode.getDefaultMsg();
    }

    public BizException(String message, IErrorCode errorCode) {
        Objects.requireNonNull(errorCode, " error code  can not be null");
        this.message = message;
        this.errorCode = errorCode;
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}

