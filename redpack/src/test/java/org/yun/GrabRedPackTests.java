package org.yun;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.yun.biz.model.RedPack;
import org.yun.redpack.PreLoadImpl;
import org.yun.redpack.TimelyImpl;

import javax.annotation.Resource;
import java.util.Date;

@SpringBootTest
class GrabRedPackTests {

    @Resource
    PreLoadImpl preLoad;


    @Resource
    TimelyImpl timely;

    private static final long userId = 123L;

    private static final long userId2 = 323L;


    @Test
    void testPreLoadCreateRed() {
        RedPack rd = new RedPack();
        rd.setAmount(1000);
        rd.setCt(new Date());
        rd.setNum(11);
        rd.setGroupId(777L);
        rd.setTitle("gongxi");
        preLoad.createRedPack(rd,userId);
    }

    @Test
    void testClick() {
        boolean result = preLoad.clickRedPack(12950806195683405L, userId);
        System.out.println(result);
    }


    @Test
    void testPreLoadGrab() {
        RedPack rd = new RedPack();
        rd.setAmount(1000);
        rd.setCt(new Date());
        rd.setNum(11);
        rd.setGroupId(777L);
        rd.setTitle("gongxi");
        Long rId = preLoad.createRedPack(rd, userId);
        Integer result = preLoad.grabRedPack(rId, userId2);
        System.out.println(result);
    }


    @Test
    void testPreLoad() {
        Integer result = preLoad.grabRedPack(12950806195683405L, userId2);
        System.out.println(result);
    }



    @Test
    void testTimelyCreateRed() {
        RedPack rd = new RedPack();
        rd.setAmount(2000);
        rd.setCt(new Date());
        rd.setNum(15);
        rd.setGroupId(888L);
        rd.setTitle("gongxi");
        Long rId = timely.createRedPack(rd, userId);
    }

    @Test
    void testTimelyClick() {
        boolean result = timely.clickRedPack(12927707005920205L, userId);
        System.out.println(result);
    }

    @Test
    void testTimelyGrab() {
        Integer result = timely.grabRedPack(12927707005920205L, userId);
        System.out.println(result);
    }


    @Test
    void testTimely() {
        RedPack rd = new RedPack();
        rd.setAmount(2000);
        rd.setCt(new Date());
        rd.setNum(15);
        rd.setGroupId(888L);
        rd.setTitle("gongxi");
        Long rId = timely.createRedPack(rd, userId);
        Integer result = timely.grabRedPack(rId, userId);
        System.out.println(result);
    }
}
