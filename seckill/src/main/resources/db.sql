CREATE TABLE `seckill_stock` (
	`sku_id` bigint(20) NOT NULL COMMENT '主键',
	`total_stock` int(11) NOT NULL DEFAULT 0 COMMENT '总库存',
	`valid_stock` int(11) NOT NULL DEFAULT 0 COMMENT '可用库存',
	`stock_type` tinyint(2) NOT NULL DEFAULT 0 COMMENT '库存类型：0普通库存；1特殊库存(具唯一性)',
	PRIMARY KEY (`sku_id`)
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='秒杀库存表'
ROW_FORMAT=COMPACT;


CREATE TABLE `seckill_order` (
	`order_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单号',
	`sku_id` bigint(20) NOT NULL COMMENT 'SKUID',
	`activity_id` bigint(20) NOT NULL COMMENT '活动ID',
	`user_id` bigint(20) NOT NULL COMMENT '用户ID',
	`order_amount` decimal(10) NOT NULL DEFAULT 0 COMMENT '订单金额',
	`order_state` tinyint(3) NOT NULL DEFAULT 0 COMMENT '订单状态',
	`pay_state` tinyint(3) NOT NULL DEFAULT 0 COMMENT '支付状态',
	`uuid` varchar(128) NOT NULL COMMENT '防重索引',
	`c_t` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
	PRIMARY KEY (`order_id`),
	 KEY `idx_orderid`(`order_id`),
	 KEY `uniq_uuid`(`uuid`),
	 KEY `idx_userid`(`user_id`)
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='秒杀订单表'
ROW_FORMAT=COMPACT;



CREATE TABLE `event_log` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `biz_id` bigint(64) NOT NULL DEFAULT 0 COMMENT '业务ID',
  `biz_code` tinyint(2) NOT NULL DEFAULT 0 COMMENT '业务码',
  `content` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '内容',
  `owner` varchar(32) NOT NULL COMMENT '数据所有者',
  `c_t` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_key_ct`(`c_t`) USING BTREE
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='事件记录表';


CREATE TABLE `id` (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sku_id` bigint NOT NULL COMMENT 'SKUID',
  `activity_id` bigint NULL COMMENT '活动ID',
   PRIMARY KEY (`id`),
   KEY `idx_skuId`(`sku_id`) USING BTREE,
   KEY `idx_activId`(`activity_id`) USING BTREE
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='sku-活动映射表';



CREATE TABLE `red_pack` (
  `red_pack_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '红包ID',
  `title` varchar(32) NOT NULL DEFAULT '恭喜发财' COMMENT '标题',
  `amount` int(5) NOT NULL DEFAULT 0 COMMENT '金额(单位分)',
  `num` int(5) NOT NULL DEFAULT 0 COMMENT '红包个数',
   `group_id` bigint NOT NULL DEFAULT 0 COMMENT '群聊ID',
   `remaining_amount` int(5)  DEFAULT 0 COMMENT '剩余金额(单位分)',
   `remaining_num` int(5)  DEFAULT 0 COMMENT '剩余红包个数',
   `c_t` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
   PRIMARY KEY (`red_pack_id`)
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='红包表';



CREATE TABLE `draw_record` (
    `tinyrd_id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '小红包ID=记录ID',
    `red_pack_id` bigint NOT NULL DEFAULT 0 COMMENT '红包ID',
    `amount` int(11) NOT NULL DEFAULT 0 COMMENT '金额（分）',
    `user_id` bigint NOT NULL DEFAULT 0 COMMENT '用户ID',
    `user_name` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名称',
    `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像',
    `draw_state` tinyint(2) NOT NULL DEFAULT 0 COMMENT '领取状态：0未领取；1已领取',
    `statements_id` bigint NOT NULL DEFAULT 0 COMMENT '对账单ID',
    `c_t` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`tinyrd_id`),
    KEY `idx_redPackId`(`red_pack_id`) USING BTREE
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='领取记录表';


CREATE TABLE `coupon` (
  `coupon_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(128) NOT NULL COMMENT '券码',
  `pwd` varchar(255) NOT NULL COMMENT '卡密',
  `state` tinyint(0) NOT NULL COMMENT '状态：0默认 1缓存内 2使用过',
  `prize_id` bigint(20) NULL DEFAULT 0 COMMENT '所属奖品ID',
  `c_t` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `u_t` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `idx_prizeCode`(`prize_id`,`code`) USING BTREE
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='券码表';




CREATE TABLE `lottery` (
 `lottery_id` bigint NOT NULL COMMENT '活动ID',
 `topic` varchar(32) NOT NULL COMMENT '主题',
 `state` tinyint NOT NULL COMMENT '状态：0默认 1上线 2下限',
 `link` varchar(255) NOT NULL COMMENT '链接',
 `images` varchar(255) NOT NULL COMMENT '图片',
 `start_time` int(11) NULL COMMENT '开始时间',
 `end_time` int(11) NULL COMMENT '结束时间',
 `c_t` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 PRIMARY KEY (`lottery_id`),
 KEY `idx_ct`(`c_t`) USING BTREE
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='抽奖活动表';




CREATE TABLE `prize` (
  `prize_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '奖品ID',
 `prize_name` varchar(32) NOT NULL COMMENT '奖品名称',
 `lottery_id` bigint NOT NULL COMMENT '活动ID',
 `prize_type` int NOT NULL COMMENT '奖品类型',
 `images` varchar(255) NOT NULL COMMENT '图片',
 `sort` tinyint(3) NOT NULL DEFAULT 0 COMMENT '序号',
 `ratio` decimal(10,8)  NOT NULL DEFAULT 0 COMMENT '中奖率',
 `total_stock` int(11) NOT NULL DEFAULT 0 COMMENT '总库存',
 `valid_stock` int(11) NOT NULL DEFAULT 0 COMMENT '剩余库存',
 `state` tinyint NOT NULL DEFAULT 0 COMMENT '状态',
 `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
 `exchange_st` int(0) NULL COMMENT '兑换开始时间',
 `exchange_et` int(0) NULL COMMENT '兑换结束时间',
 `effective_time` int(0) NULL COMMENT '生效时间',
 `expiry_time` int(0) NULL COMMENT '失效时间',
 `expiry_days` int(0) NULL COMMENT '过期天数',
 `c_t` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 `u_t` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
 PRIMARY KEY (`prize_id`)
) ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='奖品表';


CREATE TABLE `sku_activity_map` (
 `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
 `sku_id` bigint(20) NOT NULL COMMENT 'SKUID',
 `activity_id` bigint(20) NULL COMMENT '活动ID',
 PRIMARY KEY (`id`),
 KEY `idx_skuId`(`sku_id`) USING BTREE,
 KEY `idx_activId`(`activity_id`) USING BTREE
) ENGINE=InnoDB
  DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
  COMMENT='sku-活动映射表'
  AUTO_INCREMENT=1
  ROW_FORMAT=DYNAMIC
  AVG_ROW_LENGTH=0;




CREATE TABLE `lottery_record` (
  `record_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `user_id` bigint(20)  NOT NULL COMMENT '用户ID',
  `lottery_topic`  varchar(32)  NOT NULL COMMENT '活动主题',
  `prize_id` bigint(20)  NOT NULL COMMENT '奖品ID',
  `prize_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '奖品名称',
  `lottery_id` bigint(20) NOT NULL COMMENT '活动ID',
  `prize_type` int(11) NOT NULL COMMENT '奖品类型',
  `images` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片',
  `sort` tinyint(3) NOT NULL DEFAULT 0 COMMENT '序号',
  `ratio` decimal(10,8)  NOT NULL DEFAULT 0.00000000 COMMENT '中奖率',
  `total_stock` int(11) NOT NULL DEFAULT 0 COMMENT '总库存',
  `valid_stock` int(11) NOT NULL DEFAULT 0 COMMENT '剩余库存',
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '状态',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `exchange_st` int(11) NULL COMMENT '兑换开始时间',
  `exchange_et` int(11) NULL COMMENT '兑换结束时间',
  `effective_time` int(11) NULL COMMENT '生效时间',
  `expiry_time` int(11) NULL COMMENT '失效时间',
  `expiry_days` int(11) NULL COMMENT '过期天数',
  `code`  varchar(128)  NOT NULL COMMENT '券码',
  `pwd`  varchar(255)  NOT NULL COMMENT '卡密',
  `c_t` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`record_id`),
  KEY `idx_userId`(`user_id`) USING BTREE,
  KEY `idx_prizeId`(`prize_id`) USING BTREE
) ENGINE=InnoDB
  DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
    COMMENT='抽奖记录表'
  ROW_FORMAT=DYNAMIC
  AVG_ROW_LENGTH=0;


