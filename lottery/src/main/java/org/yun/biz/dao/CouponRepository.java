package org.yun.biz.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.yun.biz.model.Coupon;

import java.util.List;


/**
 * @ProjectName: no-concurrent
 * @ClassName: StockDao
 * @Description: 优惠券
 * @Author: liyunfeng31
 * @Date: 2020/10/5 0:42
 */
public interface CouponRepository extends JpaRepository<Coupon, Long> {


    /**
     * 获取指定个数的coupon
     * @param prizeId  奖品ID
     * @param limit  条数
     * @return list
     */
    @Query(value = "select coupon_id, code, pwd,state,c_t,u_t,prize_id from coupon where prize_id = ?1 and state = 0 order by coupon_id asc limit ?2",nativeQuery=true)
    List<Coupon> findByIdLimit(Long prizeId, int limit);


    /**
     * 修改券码状态
     * @param ids couponIds
     * @return eff row
     */
    @Modifying
    @Query(value = "update coupon set state = 1 where coupon_id in :ids",nativeQuery=true)
    int updateCouponState(@Param(value = "ids") List<Long> ids);
}