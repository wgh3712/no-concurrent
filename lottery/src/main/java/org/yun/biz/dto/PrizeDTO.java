package org.yun.biz.dto;

import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

/**
 * @ProjectName: no-concurrent
 * @ClassName: Prize
 * @Description: 中奖率和库存是不能返回页面的
 * @Author: liyunfeng31
 * @Date: 2020/10/7 0:46
 */
@Data
public class PrizeDTO implements Serializable {


    /**
     * 奖品ID
     */
    private Long prizeId;

    /**
     * 奖品名称
     */
    private String prizeName;

    /**
     * 奖品类型 谢谢参与-1    普通奖品 1   具有唯一性的奖品2
     */
    private Integer prizeType;

    /**
     * 图片
     */
    private String images;

    /**
     * 位置
     */
    private Integer sort;

    /**
     * 备注
     */
    private String remark;

    /**
     * 有效兑换开始时间
     */
    private Integer exchangeSt;

    /**
     * 有效兑换结束时间
     */
    private Integer exchangeEt;

    /**
     * 生效时间
     */
    private Integer effectiveTime;

    /**
     * 过期失效时间
     */
    private Integer expiryTime;

    /**
     * 兑换券码
     */
    private String code;

    /**
     * 密码
     */
    private String pwd;

}
