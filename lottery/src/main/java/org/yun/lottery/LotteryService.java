package org.yun.lottery;

import org.yun.biz.dto.PrizeDTO;
import org.yun.biz.model.Prize;

/**
 * @ProjectName: no-concurrent
 * @ClassName: Strategy
 * @Description: TODO(一句话描述该类的功能)
 * @Author: liyunfeng31
 * @Date: 2020/10/4 23:19
 */
public interface LotteryService {


  /**
   * 申请抽奖机会
   * @param userId 用户ID
   * @param lotteryId 抽奖活动ID
   * @param num 申请数量
   * @param expireTime 过期时间默认0
   * @return
   */
  Boolean applyChance(Long userId, Long lotteryId, int num, long expireTime);


  /**
   * 进行抽奖
   * @param userId 用户ID
   * @param lotteryId 抽奖活动ID
   * @return 奖品
   */
  PrizeDTO luckLottery(Long userId, Long lotteryId);
}
